package com.egs.servlets;

import com.egs.DBService.DBService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

public class LoginServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        try {
            DBService dbService=new DBService();
            if(request.getParameter("password").equals(dbService.getPassword(request.getParameter("login")))) {
                HttpSession session = request.getSession();
                session.setAttribute("user", request.getParameter("login"));
                session.setMaxInactiveInterval(30 * 60);
                Cookie userName = new Cookie("user", request.getParameter("login"));
                userName.setMaxAge(30 * 60);
                response.addCookie(userName);
                response.sendRedirect("login.jsp");
            }
            else {
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");
                PrintWriter out= response.getWriter();
                out.println("<font color=red>Either user name or password is wrong.</font>");
                rd.include(request, response);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void destroy() {

    }
}
