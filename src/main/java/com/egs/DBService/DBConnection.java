package com.egs.DBService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    private static String DB_URL = "jdbc:mysql://localhost:3306/user";
    private static String DB_USERNAME = "root";
    private static String DB_PASSWORD = "admin";
    private static String DB_DRIVER = "com.mysql.jdbc.Driver";
    private  Connection connection;
    private static DBConnection provider = new DBConnection();



    private DBConnection() {
        try {
            Class.forName(DB_DRIVER);

        } catch (Exception ex) {
        }
    }

    public static DBConnection getInstance() {
        return provider;
    }



    public  Connection getConnection() throws SQLException, ClassNotFoundException {

        if(connection==null){
            return connection=DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        }
        return connection;
    }

}
