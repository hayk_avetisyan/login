package com.egs.DBService;

import java.sql.*;

import com.egs.model.User;

public class DBService {
    private Connection connection;

    public DBService() throws SQLException, ClassNotFoundException {
        connection = DBConnection.getInstance().getConnection();
    }

    public void showUsers() throws SQLException {
        Statement stmt = connection.createStatement();

        String sql = "SELECT " + "*FROM user";
        ResultSet rs = stmt.executeQuery(sql);
        while (rs.next()) {
            System.out.println(rs.getString("name") + "\t" +
                    rs.getString("surname") + "\t" +
                    rs.getString("login") + "\t" +
                    rs.getString("password")

            );
        }
    }

    public void addUser(User user) throws SQLException {
        String sql = " insert into user (name, surname, login, password)"
                + " values (?, ?, ?, ?)";

        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, "" + user.getName());
        preparedStatement.setString(2, "" + user.getSurname());
        preparedStatement.setString(3, "" + user.getLogin());
        preparedStatement.setString(4, "" + user.getPassword());
        preparedStatement.execute();
    }

    public String getPassword(String login) throws SQLException {
        String sql = "SELECT password from user WHERE login=?";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setString(1, login);
        ResultSet rs = stmt.executeQuery();
        String password = null;
        while (rs.next()) {
            password = rs.getString("password");
        }

        return password;
    }
}

